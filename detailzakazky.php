<?php include_once "headerloggedin.php" ?>
<h1>Zakázka Z123</h1>
<form>
    <div class="row">
        <div class="large-6 columns">
            <fieldset>
                <legend>Základní údaje</legend>
                <input type="text" placeholder="Číslo zakázky">
                <input type="text" placeholder="Místo">
                <input type="text" placeholder="Datum od" >
                <input type="text" placeholder="Datum do" >
            </fieldset>
            <fieldset>
                <legend>Přidělení bezpečáci</legend>
                <div class="row collapse">
                    <div class="small-10 columns">
                        <input type="text" placeholder="Jméno / číslo bezpečáka">
                    </div>
                    <div class="small-2 columns">
                        <a href="#" class="button prefix">Přidat</a>
                    </div>
                </div>
                <table width="100%">
                    <thead>
                    <tr>
                        <th >Číslo bezpečáka</th>
                        <th >Jméno</th>
                        <th widh=50></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><a href="#">B789</a></td>
                        <td>Franta Cavrda</td>
                        <td><button class="tiny secondary alert">Odstranit</button></td>
                    </tr>
                    </tbody>
                </table>
            </fieldset>
        </div>
        <div class="large-6 columns">
            <fieldset>
                <legend>Popis</legend>
                <textarea></textarea>
            </fieldset>
            <fieldset>
                <legend>Informace o klientovi</legend>
                <div class="row collapse">
                    <div class="small-10 columns">
                        <input type="text" placeholder="Název / číslo klienta">
                    </div>
                    <div class="small-2 columns">
                        <a href="#" class="button prefix">vybrat</a>
                    </div>
                </div>
                <div class="row">
                    <div class="large-4 columns">
                        <p><b>Název</b></p>
                    </div>
                    <div class="large-8 columns">
                        <p>Komtesa s.r.o.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="large-4 columns">
                        <p><b>Adresa</b></p>
                    </div>
                    <div class="large-8 columns">
                        <p>Havlíčkova 2034</p>
                    </div>
                </div>
                <div class="row">
                    <div class="large-4 columns">
                        <p><b>Telefon</b></p>
                    </div>
                    <div class="large-8 columns">
                        <p>777967175</p>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <input type="submit" class="right button success" value="Uložit">
    <input type="submit" class="right button secondary" value="Storno">
</form>
<?php include_once "footer.php" ?>