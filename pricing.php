<?php include_once "header.php" ?>
<div class="row">
    <div class="large-4 columns">
        <ul class="pricing-table">
            <li class="title">Starter</li>
            <li class="price">0 Kč</li>
            <li class="description">An awesome description</li>
            <li class="bullet-item">1 Database</li>
            <li class="bullet-item">5GB Storage</li>
            <li class="bullet-item">20 Users</li>
            <li class="bullet-item">
                <input type="checkbox" checked>
            </li>
        </ul>
    </div>
    <div class="large-4 columns">
        <ul class="pricing-table">
            <li class="title">Standard</li>
            <li class="price">1000 Kč</li>
            <li class="description">An awesome description</li>
            <li class="bullet-item">1 Database</li>
            <li class="bullet-item">5GB Storage</li>
            <li class="bullet-item">20 Users</li>
            <li class="bullet-item">
                <input type="checkbox">
            </li>
        </ul>
    </div>
    <div class="large-4 columns">
        <ul class="pricing-table">
            <li class="title">Enterprise</li>
            <li class="price">3000 Kč</li>
            <li class="description">An awesome description</li>
            <li class="bullet-item">1 Database</li>
            <li class="bullet-item">5GB Storage</li>
            <li class="bullet-item">20 Users</li>
            <li class="bullet-item">
                <input type="checkbox">
            </li>
        </ul>
    </div>
    <input type="submit" class="large button expand" value="Dokončit registraci">
</div>
<?php include_once "footer.php" ?>