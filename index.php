<?php include_once "header.php" ?>

<div class="row">
    <div class="large-12 columns">
        <ul data-orbit>
            <li>
                <img src="http://lorempixel.com/output/business-q-c-970-300-2.jpg" />
                <div class="orbit-caption">CRM pro Vás</div>
            </li>
            <li>
                <img src="http://lorempixel.com/output/business-q-c-970-300-6.jpg" />
                <div class="orbit-caption">Snadná evidence zakázek</div>
            </li>
            <li>
                <img src="http://lorempixel.com/output/business-q-c-970-300-8.jpg" />
                <div class="orbit-caption">Informace na dosah ruky</div>
            </li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="large-8 columns">
        <div class="panel">
            <h1>CRM pro bezpečnostní agentury</h1>

            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut
                laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation
                ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>

            <p> Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum
                dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent
                luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis
                eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>

            <p> Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes
            demonstraveru</p>
        </div>
    </div>
    <div class="large-4 columns">
        <form action="registration.php">
            <fieldset>
                <legend>Registrace</legend>
                <input type="text" placeholder="Název firmy">
                <input type="email" placeholder="Email">
                <input type="password" placeholder="Heslo">
                <input type="password" placeholder="Heslo znovu">
                <input type="submit" class="large button expand" value="Registrovat">
            </fieldset>
        </form>
    </div>
</div>

<?php include_once "footer.php" ?>
