<!DOCTYPE html>
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en"> <!--<![endif]-->

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width"/>
    <title>Foundation 4</title>

    <link rel="stylesheet" href="stylesheets/normalize.css"/>

    <link rel="stylesheet" href="stylesheets/app.css"/>

    <script src="javascripts/vendor/custom.modernizr.js"></script>

</head>
<div class="row">
    <div class="large-12 columns">
        <h2>BezAgen</h2>

        <p>...</p>
        <hr/>
    </div>
</div>
<div class="row">
    <div class="large-12 columns">
        <nav class="top-bar" style="">
            <section class="top-bar-section">
                <ul class="left">
                    <li><a href="index.php">Úvod</a></li>
                    <li class="divider"></li>
                    <li><a href="html">...</a></li>
                </ul>
                <!-- Right Nav Section -->
                <ul class="right">
                    <li class="divider hide-for-small"></li>
                    <li><a href="#" >Josef Novák [Admin]</a></li>
                    <li class="divider"></li>
                    <li><a href="index.php">Odhlásit</a></li>
                </ul>
            </section>
        </nav>
    </div>
</div>
<div class="row">
    <div class="large-12 columns">